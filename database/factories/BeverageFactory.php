<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    $taken_off = $faker->dateTimeBetween('-20 days', '+0 days');
	$taken_off = Carbon::createFromDate($taken_off->format('Y'), $taken_off->format('m'), $taken_off->format('d'));
	$end = Carbon::createFromDate($taken_off->format('Y'), $taken_off->format('m'), $taken_off->format('d'));
	$end_2 = Carbon::createFromDate($taken_off->format('Y'), $taken_off->format('m'), $taken_off->format('d'));
    $best_before = $end->addDays(rand(3, 7));
    $donated_on = $end_2->addDays(4,9);
    return [
        'name' => $faker->beverageName(), 
        'category' => 'beverage', 
        'best_before' => $best_before, 
        'other_data' => 'text', 
        'taken_off_the_shelf_date' => $taken_off,
        'donated_on' => $donated_on
    ];
});
