<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('store_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->dateTime('ordered_at');
            $table->dateTime('donation_expiry_date');
            $table->timestamps();
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donations', function (Blueprint $table) {
            if (Schema::hasColumn('donations', 'store_id'))
            {
                Schema::table('donations', function (Blueprint $table) {
                    $table->dropForeign('donations_store_id_foreign');
                    $table->dropForeign('donations_user_id_foreign');
                });
            }
        });
        Schema::dropIfExists('donations');
    }
}
