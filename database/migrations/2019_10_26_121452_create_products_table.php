<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('store_id')->unsigned();
            $table->string('name');
            $table->string('category')->nullable();
            $table->timestamp('best_before')->nullable();
            $table->timestamp('use_by')->nullable();
            $table->timestamp('taken_off_the_shelf_date')->nullable();
            $table->timestamp('donated_on')->nullable();
            $table->boolean('is_donated')->default(0);
            $table->text('other_data')->nullable();
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            if (Schema::hasColumn('products', 'store_id'))
            {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropForeign('products_store_id_foreign');
                });
            }
        });
        Schema::dropIfExists('products');
    }
}
