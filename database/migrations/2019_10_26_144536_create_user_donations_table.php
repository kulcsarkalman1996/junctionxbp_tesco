<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_donations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('donation_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('user_donations', function (Blueprint $table) {
            $table->foreign('donation_id')->references('id')->on('donations')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_donations', function (Blueprint $table) {
            if (Schema::hasColumn('user_donations', 'store_id'))
            {
                Schema::table('user_donations', function (Blueprint $table) {
                    $table->dropForeign('user_donations_donation_id_foreign');
                    $table->dropForeign('user_donations_user_id_foreign');
                });
            }
        });
        Schema::dropIfExists('user_donations');
    }
}
