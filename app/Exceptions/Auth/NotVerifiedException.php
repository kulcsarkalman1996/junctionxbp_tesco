<?php

namespace App\Exceptions\Auth;

use Exception;
use App\Exceptions\BaseException;

class NotVerifiedException extends BaseException
{
    protected $message = "User has not been verified.";
    protected $statusCode = 401;
}
