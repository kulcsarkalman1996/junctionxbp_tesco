<?php

namespace App\Exceptions;

use App\Exceptions\BaseException;

class InvalidRequestData extends BaseException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return $this->respondInvalidRequestData($this->message);
    }
}
