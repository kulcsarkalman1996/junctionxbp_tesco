<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'category', 'best_before', 'use_by', 'is_donated', 'other_data', 'store_id', 'taken_off_the_shelf_date', 'donated_on'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function store() 
    {
        return $this->belongsTo(Store::class);
    }
}
