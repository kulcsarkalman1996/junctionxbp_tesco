<?php

namespace App\Http\Controllers\Auth;

use Laravel\Passport\Http\Controllers\AccessTokenController;
use App\Exceptions\Auth\InvalidCredentialsException;
use App\Http\Requests\User\UserRegisterRequest;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response as Psr7Response;
use App\Exceptions\Auth\NotExistException;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class AuthController extends AccessTokenController
{
    public function issueToken(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        $this->validate($input);
        $email = $input['username'];
        $password = $input['password'];

        try {
            $oAuthClient = DB::table('oauth_clients')->where('name', 'LIKE', env('PASSPORT_API_CLIENT', '%Password Grant Client'))->first();
            $request = $request->withParsedBody([
                'grant_type' => 'password',
                'client_id' => @$oAuthClient->id,
                'client_secret' => @$oAuthClient->secret,
                'username' => $email,
                'password' => $password,
            ]);
            $user = $this->user($email);
            if($user)
            {
                $data = json_decode($this->convertResponse($this->server->respondToAccessTokenRequest($request, new Psr7Response))->content(), true);
            }
            else
                throw new NotExistException();
        } catch (\League\OAuth2\Server\Exception\OAuthServerException $exception) {
            if ($exception->getErrorType() === 'invalid_credentials') {
                $user = $this->user($email);
                if (empty($user)) {
                    throw new InvalidCredentialsException();
                }
                if (!Hash::check($password, $user->password)) {
                    throw new InvalidCredentialsException();
                }
            }
            throw $exception;
        }
        $data['user_role'] = $user->role;
        return $data;
    }

    public function user(string $email)
    {
        return User::where('email', $email)->first();
    }

    public function validate(array $input)
    {
        validator()->make($input, [
            'username' => 'required|email',
            'password' => 'required|string',
        ])->validate();
    }

    public function register(UserRegisterRequest $request) {

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        return $user;
    }
}
