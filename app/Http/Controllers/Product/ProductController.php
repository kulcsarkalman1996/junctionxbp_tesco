<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateProductFormRequest;
use App\Product;
use App\Store;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductFormRequest $request)
    {
        $existing_id = $this->storeExists($request->tesco_store_id);
        $data = $request->all();
        if ($existing_id) {
            $data['store_id'] = $existing_id;
            $product = Product::create($data);
            return $product;
        } else {
            $new_store = Store::create($request->only('tesco_store_id'));
            $data['store_id'] = $new_store->id;
            $product = Product::create($data);
            return $product;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function storeExists(string $tesco_store_id)
    {
        $store = Store::where('tesco_store_id', $tesco_store_id)->first();
        if (gettype($store) == 'NULL') {
            return false;
        }
        return $store->id;
    }
}
